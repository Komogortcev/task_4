from datetime import datetime, timedelta
print("Изменение в файле")
try:
    print('Введите дату дня рождения ДД/ММ/ГГГГ')
    hb_date = datetime.strptime(input(), "%d/%m/%Y")
except:
    print('Дата в неверном формате')
    exit()

day_time = timedelta(days = 10000)
min_time = timedelta(minutes = 1000000)
sec_time = timedelta(seconds = 1000000000)
day_date = hb_date + day_time
min_date = hb_date + min_time
sec_date = hb_date + sec_time
print(f'Дата, когда исполнится 10 000 дней: {day_date}')
print(f'Дата, когда исполнится 1 000 000 минут: {min_date}')
print(f'Дата, когда исполнится 1 000 000 000 секунд: {sec_date}')